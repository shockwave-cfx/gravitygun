-- SetPlayerTargetingMode

local INPUT_PICKUP = 38

local function TargetLoop()
  local player = PlayerId()
  local playerPed = PlayerPedId()
  local hasWeapon, currentWeapon = GetCurrentPedWeapon(playerPed)
  local isTargeting, targetEntity = GetEntityPlayerIsFreeAimingAt(player)

  if not IsPlayerFreeAiming(player) then
    return
  end

  if not hasWeapon or currentWeapon ~= GetHashKey('WEAPON_STUNGUN') then
    return
  end

  if controlledEntity then
    if IsControlJustReleased(0, INPUT_PICKUP) then
      controlledEntity = nil
      print('drop')
    end
  elseif isTargeting then
    if IsControlJustPressed(0, INPUT_PICKUP) then
      controlledEntity = targetEntity
      print('grab')
    end
  end

  -- print('target: ', tostring(targetEntity))
end

Citizen.CreateThread(function ()
  while true do
    Citizen.Wait(0)
    TargetLoop()
  end
end)
